# JavaScript

Grundlagen Datenbanken und angrenzende Themen.

Konzipiert als Kurs und Nachschlagewerk.

Die aktuelle Build-Version ist direkt abrufbar unter:

https://oer-kurse.gitlab.io/javascript/

Andere Kursinhalte sind zu finden über:  https://oer-kurse.gitlab.io

Peter Koppatz (2022)
