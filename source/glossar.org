:PROPERTIES:
:ROAM_ALIASES: "Glossar"
:ID:       f3ddce5d-1c9a-4298-9a2a-1fa9c592f0a6:
:END:
#+title:       Glossar
#+date:        2024-06-01 Sa 19:11]
#+identifier:  20240601T191114
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _f3ddce5d-1c9a-4298-9a2a-1fa9c592f0a6:
.. _glossar:
.. INDEX:: Glossar; JavaScript
.. INDEX:: JavaScript; Glossar


:B:
   **BDD** -- Behavior Driven Development

   **Boilerplate** -- vorbereiteter Quellcode und/oder Ordnerstrukturen

:C:

   **CJS** -- CommonJS  
   
:D:

   **DOM** -- document object model

:E:

   **ECMA** -- European Computer Manufacturers Association

   **ECMAScript**

   Warum verwenden wir ECMA oder kurz ES als Bezeichnung?
   
   1995 ging Netscape eine Partnerschaft mit Sun Microsystems ein,
   um interaktive Websites zu erstellen. Brendan Eich hat in nur
   zehn Tagen, die erste Version von JavaScript erstellt. Als Ergebnis
   dieser Partnerschaft wechselte die Marke »JavaScript« zur Firma
   Sun. Oracle kaufte 2009 Sun Microsystems und damit auch die
   JavaScript-Marke.
      
   **End-to-End Test**
   
   Definition - What does End-to-End Test mean?

   End-to-end testing is a methodology used to test whether the flow
   of an application is performing as designed from start to
   finish. The purpose of carrying out end-to-end tests is to identify
   system dependencies and to ensure that the right information is
   passed between various system components and systems.
   
   Techopedia explains End-to-End Test

   End-to-end testing involves ensuring that that integrated components
   of an application function as expected. The entire application is
   tested in a real-world scenario such as communicating with the
   database, network, hardware and other applications.
   

   For example, a simplified end-to-end testing of an email
   application might involve:

   - Logging in to the application
   - Accessing the inbox
   - Opening and closing the mailbox
   - Composing, forwarding or replying to email
   - Checking the sent items
   - Logging out of the application

   :Quelle: https://www.techopedia.com/definition/7035/end-to-end-test

   **ESM** -- ES Modules/EcmaScript modules, ist ein standardidisiertes Module-System für JavaScript
	    
:H:

    **HMR** -- Hot Module Replacement
:I:

   **IIFEs** -- Immediately Invoked Function Expressions 
:J:
   **JAMStack** Akronym für  JavaScript, APIs and Markdown

   Die Idee ist, das Seitenmarkup für die gesamte Website zum
   Zeitpunkt der Erstellung zu generieren und nicht erst zum Zeitpunkt
   des Aufrufs durch den Nutzer.

   Eine Übersicht zu verfügbaren Software-Lösungen bietet:

   https://jamstack.org/generators/
   
   **JSON** -- JavaScript Object Notation

:M:

   **monkey patching** -- ändern von Eigenschaften an allen schon existierenden/laufenden Objekten/Programmen.

   .. index:: MVC
	   
   **MVC** -- Model-View-Controller

   .. image:: ./_static/mvcc.svg

   .. index:: OOP; Gesetz von Demeter
	   
   Siehe auch: `Gesetz von Demeter (LoD) <https://de.wikipedia.org/wiki/Gesetz_von_Demeter>`_

.. @startuml
.. (*) --> "View"
.. --> [informiert] Controller
.. Controller --> [aendert] Model
.. Model -right-> [informiert] "View"
.. Controller --> "services"
.. @enduml

:N:

   **NPM** -- Der Node Package Manager
   
   NPM Inc. ist die Firma, die das NPM-Repository betreibt.

   **NPX** -- Node Package Execute

   Führt Pakete aus und löst Abhängigkeiten auf (lädt sie automaitsch herunter).
   
:R:

   **REPL** -- read-execute-print loop
   
   **Refactoring** -- Prozess der Überarbeitung von Quellcode
   
:S:

   **SFC** Single File Component
   
   **SPA** -- single page application

   **Sprachindex**

   - `Sprachen: <https://www.tiobe.com/tiobe-index>`_
   - `Webframeworks I <https://insights.stackoverflow.com/survey/2021\#section-most-loved-dreaded-and-wanted-web-frameworks>`_ 
   - `Webframeworks II <https://insights.stackoverflow.com/survey/2021#section-most-popular-technologies-web-frameworks>`_

   **SSR** serverseitiges Rendern (server side rendering)
     
:T:

   
   **TC39** -- Ist eine Gruppe von JavaScript-Entwicklern,
   -Implementierern, -Wissenschaftlern und anderen, die mit
   der Gemeinschaft zusammenarbeiten, um die Definition von
   JavaScript zu pflegen und weiterzuentwickeln.

   **Typosquatting** -- Angriffsart auf Software, die durch
   öffentliche Repositories, wie npm (JavaScript) oder PyPi (Python)
   verwaltet und der Allgemeinheit zur Verfügung gestellt wird.
   Dabei wird einer bekannten Bibliothek, eine mit fast
   gleichlautendem Namen zur Seite gestellt, z.B. über den Austausch
   von Bindestrich durch Unterstrich oder durch anhängen eines
   Buchstabens. Wird durch eine Schreibfehler die schadhafte Version
   geladen, öffnet sich für den Angreifer eine Tür.
   

:W:

   **Web5** -- damit man mitreden kann (Oder ist das die Zukunft?):

   »Web 2« + »Web 3« --> »Web 5«
   
   Siehe auch:

   - `Artikel zu Web5 <https://www.vice.com/en/article/epzgwn/jack-dorsey-fuck-it-were-doing-web5>`_
   - `Google-Präsentation <https://docs.google.com/presentation/d/1SaHGyY9TjPg4a0VNLCsfchoVG1yU3ffTDsPRcU99H1E/preview?pru=AAABgXL3neo*ps8myfj7XNPWRTHpEkckGA#slide=id.g11b904107df_0_1>`_
:Y:

   **YARN**  Yet Another Resource Negotiator

   Ein besseress(?) NPM der Firma Facebook.
   
#+END_export
