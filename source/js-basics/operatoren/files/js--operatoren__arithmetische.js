
let i = 10;
let x = 1 / 2;
i++              // i hat nun den Wert 11
i -= 2           // i hat nun den Wert 9
i * 2            // i hat nun den Wert 18

x = 18;
let a = x % 2   // a hat den Wert 0, weil 18/2 = 9 Rest 0

console.log('a = ', a, 'x= ', x, 'i= ', i);
console.log(`a = ${a}, x = ${x}, i = ${i} `);
