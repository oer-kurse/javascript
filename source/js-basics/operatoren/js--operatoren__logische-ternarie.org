:PROPERTIES:
:ROAM_ALIASES: "Ternary"
:ID:           aee17d11-0a78-4aa4-92b9-c7a9e61e6836
:END:
#+title:       Ternary
#+date:        [2024-05-09 Do 10:04]
#+identifier: 20240509T100446
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org
[[id:27470f9f-90e1-4dab-a792-9c70c2dac1c4][Operatoren]]
#+end_org
#+BEGIN_export rst
.. _aee17d11-0a78-4aa4-92b9-c7a9e61e6836:
.. INDEX:: Operatoren; ternary
.. INDEX:: ternary; Operatoren
#+END_export

Kondition (conditional/ternary operator)

Wenn die Bedingung links vom Fragezeichen wahr ist, dann die Operation
rechts vom Fragezeichen ausführen, andernfalls die Operation hinter
dem Doppelpunkt.

#+begin_src javascript
  x > 0 ? x*y : -x*y
#+end_src
   
   
#+begin_src javascript

  const zahl = 11
  zahl % 2 == 0 ? console.log("gerade") : console.log("ungerade")
 
#+end_src
 
 
