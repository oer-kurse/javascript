:PROPERTIES:
:ROAM_ALIASES: "Klassen"
:ID:           b305e5b6-4429-4af6-9706-402da83e2653:
:END:
#+title:       Klassen
#+date:        2024-05-28 Di 21:28]
#+identifier:  20240528T212856
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org
#+end_org
#+BEGIN_export rst
.. _b305e5b6-4429-4af6-9706-402da83e2653:
.. INDEX:: Klassen
.. INDEX:: class
#+END_export

#+begin_src javascript
   
   function FPerson() {
      // Objekt Person über Funktion
   }

   const f_person = new FPerson()
   console.log(f_person)


   class CPerson {
     gruss() {
      console.log("Hallo aus der Klasse")
     }
   }

   const c_person = new CPerson();
   console.log(c_person);


   class Person {
     constructor(name){
       this.name = name
     }

     gruss() {
       console.log("Hallo aus Klasse mit Konstruktor");
     }
   }

   const person = new Person('Peter');
   console.log(person);
#+end_src

** Prototypes

Objekten wird ein Prototype zugwiesen, aber von welchem Objekt? 

#+begin_export rst

.. INDEX:: class; Prototype

console.log(person.__proto__ == Object.prototype);   // false
console.log(person.__proto__ == Person.prototype);    // true

#+end_export


** Vererbung

Wenn OOP, dann auch Vererbung.

#+begin_src javascript

  .. INDEX:: class; Vererbung

     class Sohn extends Person {}

     let sohn = new Sohn('David');
     console.log(sohn.gruss());

     class Tochter extends Person {

       constructor(alter){
         // Aufruf des Konstruktors der Superklasse
         super('Nadin');
         // Konstruktoraufrufe für die eigene Klasse
         this.alter = alter
       }

       doppelGruss() {
         super.gruss();   // direkter Aufruf der Superklasse 
         super.gruss();
     
       }
     }

     const  k2 = new Tochter(43);
     console.log(k2.doppelGruss());
#+end_src


** Static Method
#+begin_export rst

.. INDEX:: class; static Method

#+end_export
	   
Diese Methoden können nicht auf Instanzen angewendet werden,
nur auf die Klasse selbst.

#+begin_src javascript
   
   class Logger {

     static logIt(message) {
       console.log(message);
     }
   }

   Logger.logIt('Aufruf erfolgreich?, Ja');
#+end_src


   #+begin_src javascript

     class Logger2 {

       logIt(message) {
         console.log(message);
       }
     }

     // Fehler, denn ohne »static« muss eine Instanz erzeugt werden.
     // TypeError: Logger2.logIt is not a function
     // Logger2.logIt('Aufruf erfolgreich?'); 
     // das sollte funktionieren:

     const log = new Logger2()
     log.logIt('Aufruf erfolgreich?, Ja')
     
   #+end_src
