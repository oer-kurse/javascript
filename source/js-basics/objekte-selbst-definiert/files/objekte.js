let obj =  {"Name": "Peter",
            "Nachname": "Koppatz"};

let obj_as_str = '{ "Name": "Norbert", \
                    "Nachname": "Koppatz"}';

obj_from_str = JSON.parse(obj_as_str);

console.log(typeof obj_as_str, obj_as_str);
console.log(typeof obj_from_str, obj_from_str);
console.log(obj_from_str['Name']);