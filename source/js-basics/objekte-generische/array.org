:PROPERTIES:
:ROAM_ALIASES: "Arrays"
:ID:           adcbb481-72be-4ee1-b665-63c200fd4ada:
:END:
#+title:       Arrays 
#+date:        2024-05-24 Fr 18:45]
#+identifier:  20240524T184559
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _adcbb481-72be-4ee1-b665-63c200fd4ada:
.. INDEX:: Arrays; JavaScript
.. INDEX:: JavaScript; Arrays
.. INDEX:: Array; Objekt
.. INDEX:: Objekt; Array

.. image:: ./images/aufstieg-roemerfels.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/aufstieg-roemerfels.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/aufstieg-roemerfels.webp">
   </a>

.. sidebar:: JavaScript-Kurs

   |b|

   Aufstieg zum Römerfels

|a|

#+END_export

	   
Ein Array besteht aus einer durch Kommata getrennten Liste von Werten.
Array können verschachtelt werden, also wieder Arrays bzw. Objekte enthalten.

   #+begin_src javascript
     
     const arr = ["Array", "mit", 4, "Werten"]
     const arr2 = new Array()
   #+end_src
   
*Array destruction (ES6)*

Mit der Destruktion und dem »spread operator« werden die Werte
expandiert und zugeordnet, wobei die drei Punkte vor dem Bezeichner
eine beliebige Anzahl von Elementen, sozusagen den Rest, repräsentiert.

#+begin_src javascript

   const zahlen = [9,8,7,6,5,4,3,2,1];

   let [a, b] = zahlen;
   let [c, ...d] = zahlen;
   console.log('a: ',a);
   console.log('b: ',b);
   console.log('c: ',c);
   console.log('d: ',d);

   // Ausgaben

   a:  9
   b:  8
   c:  9
   d:  [ 8, 7, 6, 5,
         4, 3, 2, 1
       ]
#+end_src


*Arrays für Matrizenrechnung*

Ein Beispiel für ein verschachteltes Array.

#+begin_src javascript

   const matrix = [
     [1, 2, 3],
     [4, 5, 6],
     [7, 8, 9]
   ]

   matrix[0][0] //1
   matrix[2][0] //7
#+end_src


*Weitere Methonden für Array-Objekte:*

#+begin_export rst
- :ref:`Prototype zur Beschreibung von Objekteigenschaften <array_methods>`
#+end_export
- https://www.w3schools.com/js/js_arrays.asp
- `Vergleich des Laufzeitverhaltens beim füllen großer Arrays
  <https://annoyscript.vercel.app/posts/The%20fastest%20way%20to%20work%20with%20arrays/>`_
