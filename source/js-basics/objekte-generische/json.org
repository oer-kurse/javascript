:PROPERTIES:
:ROAM_ALIASES: "JSON"
:ID:           e849aa84-feca-483f-8036-b5c600a4332c:
:END:
#+title:       JSON
#+date:        2024-05-24 Fr 19:30]
#+identifier:  20240524T193032
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+BEGIN_export rst
.. _e849aa84-feca-483f-8036-b5c600a4332c:
.. INDEX:: JSON; JavaScript 
.. INDEX:: JavaScript; JSON

.. image:: ./images/bank-style-02.webp
   :width: 0px

.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bank-style-02.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bank-style-02.webp">
   </a>

.. sidebar:: JavaScript-Kurs

   |b|
  
|a|

#+END_export

*JSON* steht für JavaScript Object Notation.

- ist ein Satz von Regeln --> Standard
- Als Objektrepräsentation im Textformat für den Datenaustausch
  entworfen.
- Es kann Objekte »{}« und/oder Arrays »[]« enthalten.
- Funktionen und Methoden werden **nicht** serialisiert

- JSON online Prüfen: https://jsonlint.com
- Siehe auch :ref:`Beispiele für die Umwandlung von Datentypen <json>`

** JSON ist einfach und übersichtlich

Wirklich?

#+begin_src javascript
let eintrag = `
   {
    "glossary": {
        "title": "example glossary",
        "GlossDiv": {
            "title": "S",
            "GlossList": {
     	   "GlossEntry": {
     	       "ID": "SGML",
     	       "SortAs": "SGML",
     	       "GlossTerm": "Standard Generalized Markup Language",
     	       "Acronym": "SGML",
     	       "Abbrev": "ISO 8879:1986",
     	       "GlossDef": {
     		   "para": "A meta-markup language, used to create markup languages such as DocBook.",
     		   "GlossSeeAlso": ["GML", "XML"]
     	       },
     	       "GlossSee": "markup"
     	   }
            }
        }
    }
}`

let glossary_obj = JSON.parse(eintrag)
#+end_src

** JSON-Beispiele
   
Viele Varianten von Name-Wert-Paaren in in einem Array
(entnommen einer Python-Testdatei).

#+begin_export rst
   
.. literalinclude:: ./files/json-test.js
   :language: javascript
   
#+end_export

