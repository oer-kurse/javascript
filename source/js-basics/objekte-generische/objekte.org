:PROPERTIES:
:ROAM_ALIASES: "Objekte -- generische"
:ID:       face7d77-3f66-43cc-b1b4-6fcbdba6ef0d:
:END:
#+title:       Objekte -- generische
#+date:        2024-05-24 Fr 19:33]
#+identifier:  20240524T193338
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _face7d77-3f66-43cc-b1b4-6fcbdba6ef0d:
.. INDEX:: Objekte; generische
.. INDEX:: generische; Objekte

.. meta::

   :description lang=de: Objekte in JavaScript
   :keywords: object, values, entries

.. INDEX:: Objekte; generische
.. INDEX:: generische; Objekte

.. image:: ./images/bank-style-02.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/bank-style-02.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/bank-style-02.webp">
   </a>

.. sidebar:: JavaScript-Kurs

   |b|

|a|
#+END_export
	   
Objekt kapseln ein spezielles Datum oder sind Sammlungen von Daten
bzw. Name-Wert-Paare.

- Object
- Array
- Date
- RegExpr

Wie bei einer Klasse enthalten sie Daten und Methoden.

Neuanlage oft mit dem Schlüsselwort »new«

#+begin_src javascript

   o = new Object;
   d = new Date();
   a = new Array()

#+end_src

#+begin_export rst
.. literalinclude:: ./files/objekte.js
   :language: js
#+end_export


*Objekte: Values als Array*

#+begin_export rst
.. INDEX:: Objekt; Werte als Array
.. INDEX:: Werte als Array; Objekt
#+end_export

#+begin_src javascript

   let obj =  {"Name": "Peter",
               "Nachname": "Koppatz"};
   Object.values(obj);
  
   // [ 'Peter', 'Koppatz' ]
#+end_src

*Objekte: Name/Werte*
#+begin_export rst

.. INDEX:: Objekt; Name/Wert-Paare als Aarray
.. INDEX:: Name/Wert-Paare als Aarray; Objekt
#+end_export

#+begin_src javascript

   let obj =  {"Name": "Peter",
               "Nachname": "Koppatz"};
   Object.entries(obj);
   
   // [['Name', 'Peter'], ['Nachname', 'Koppatz']]
#+end_src
*Inhalte des Objektes*

Quelle: [[https://stackoverflow.com/questions/15690706/recursively-looping-through-an-object-to-build-a-property-list][Beispiel mit Rekursion von Stackoverflow]]

#+begin_export rst
.. INDEX:: Objekt; Key-Values
.. INDEX:: Key-Values; Objekt
.. INDEX:: Rekursion; Objekthirarchie
.. INDEX:: Objekthirarchie; Rekursion
#+end_export
   
#+begin_src javascript
   function iterate(obj, stack) {
	   for (var property in obj) {
	       if (obj.hasOwnProperty(property)) {
		   if (typeof obj[property] == "object") {
		       iterate(obj[property], stack + '.' + property);
		   } else {
		       console.log(property + "   " + obj[property]);
		   }
	       }
	   }
       }

   iterate(glossary_obj, '')
#+end_src

oder

#+begin_export rst

.. INDEX:: Objekt; flat
.. INDEX:: flat; Objekt
#+end_export

#+begin_src javascript

   // npm install flat
   var flatten = require('flat')
   flatten(glossary_obj)

   #+end_src
   
 
