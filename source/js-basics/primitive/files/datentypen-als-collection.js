var collection = {
  string: "I'm a string!",
  number: 123e-5,
  float: 1.23456789 - (1.23456789 % 0.2),
  boolean: true,
  array: [0, "1", "2"],
  object: {
    array: [[0], ["1", "2"]],
  },
  null: null,
  infinity: Infinity,
  nan: NaN,
  undefined: undefined,
  function: function () {
    return false;
  },
};

console.log(collection);

for (var key in collection) {
  // Ausgabe vorbereiten
  text = "";
  text += "\nReserviertes Wort (key) ist: " + key;
  text += "\n- Beispielwert ist: " + collection[key];
  text += "\n- Datentyp ist: " + typeof collection[key];
  console.log(text);
}
