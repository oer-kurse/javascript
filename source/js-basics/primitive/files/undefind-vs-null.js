let a = [ 'a', 'b', 'c' ];
delete a[1];

for (var i = 0; i < a.length; i++)
    console.log(i + ": " + a[i]);

a[1] = null;

for (var i = 0; i < a.length; i++)
    console.log(i + ": " + a[i]);

console.log(typeof null);
console.log(typeof undefinded);


let a = ''; 
console.log(typeof a); // string 
console.log(a == null); //false 
console.log(a == undefined); // false 


let b;
console.log(b == null); //true
console.log(b == undefined); //true 

let c; 
console.log(c === null); //false 
console.log(c === undefined); // true
