let  headline = 'Lotto';
let  msg = 'Ist das der grosse Gewinn?';
let  anzahl = 6;
let  min =  1;
let  max =  49;
let  zahlen = [1,2,3,4,5,6,7];

// Ausgabe als REPL

console.log(headline);
console.log("Anzahl: " + anzahl);
console.log ("min und max:  " + min + ",  " + max);

// Ausgabe in eine Datei

const fs = require('fs');


let data = headline + 
    "\nAnzahl: " + anzahl +
    "\nmin und max:  " + min + ",  " + max +
    '\n'

fs.writeFileSync('var2txt.txt', data);
