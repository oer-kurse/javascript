let obj06 = {
    gruss: "06: Hallo Du Pappnase!",
    machwas: function() {
      console.log(this.gruss);
      this.was_anderes();
    },
    was_anderes: function() {
      console.log(this.gruss.split("").reverse().join(""));
    }
  };
  obj06.machwas(); 

  let Macher = function() {
    this.gruss  = "07 Hallo Du Pappnase!";
    this.machwas = function() {
      console.log(this.gruss);
      this.was_anderes();
    };
    this.was_anderes = function() {
      console.log(this.gruss.split("").reverse().join(""));
    }
  };


  let mach1 = new Macher(); 
  console.log(mach1.machwas());
  let mach2 = new Macher(); 
  console.log(mach2.machwas());