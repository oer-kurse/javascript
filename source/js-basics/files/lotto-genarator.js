let lottozahlen = {
  [Symbol.iterator]: gen
}


function *gen() {
  for (let i = 0; i < 6; i++) {
    yield [i+1, Math.floor((Math.random() * 49) + 1)];
  }
}

for (let zahl of lottozahlen){
 console.log(zahl);
}



function *ziehung(max) {
    for (let i = 0; i < max; i++) {
	yield [i+1, Math.floor((Math.random() * 49) + 1)];
    }
}

let w12 = ziehung(6)
console.log(w12)
