let collection = {

  string: 'I\'m a string!',
  number: 123e-5,
  float: 1.23456789 - (1.23456789 % 0.2),
  boolean: true,
  array: [0,'1', '2'],
  object: {
    array: [
      [0],
      ['1', '2']
    ]
  },
  null: null,
  infinity: Infinity,
  nan: NaN,
  undefined: undefined,
  function: function() {
    return false;
  }
};

console.log(collection)

for (var prop in collection) {
  console.log(prop + " = " + collection[prop] + " << type ist: " + typeof(collection[prop]));
}
