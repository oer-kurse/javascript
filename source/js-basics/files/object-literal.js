// objekte defineiren

let obj = {
  "name": 'Peter',
  "alter": 65,
    "spruch": "Kaum macht man's richtig, funktioniert's...",
    "sag mal"(){
	console.log(this.name + ": " + this.spruch);
    }
};

console.log(obj);
obj["sag mal"]()

// Objekte entpacken

let {name, alter} = obj;

console.log(name);
console.log(alter);
console.log(obj.name);
console.log(obj.alter);
