let ersterWert = 999;

function ersteFunktion() {
    let zweiterWert = ersterWert + 1;
    return zweiterWert;
}
module.exports = {
    ersterWert: ersterWert,
    ersteFunktion: ersteFunktion
}
