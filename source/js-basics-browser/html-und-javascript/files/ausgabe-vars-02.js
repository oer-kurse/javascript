const Person = function (vname, nname) {
    this.vname = vname,
    this.nname = nname
};

p1 = new Person('Peter', 'Koppatz')
console.log(p1);

Person.prototype.fullname = function () {
    let fname = `Vor- und Zuname: ${
        this.nname
    }, ${
        this.vname
    } `
    return fname
}

console.log(p1.fullname())

let ausgabe = document.querySelector("#debug")
let text = ""
text += p1.fullname()
text += '\n\n'
text += JSON.stringify(p1)
ausgabe.textContent = text
