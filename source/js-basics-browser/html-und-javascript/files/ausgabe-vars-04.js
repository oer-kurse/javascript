let ausgabe = document.querySelector("#debug")
let text = "" // JSON.stringify(team[5])

// Ausgabe Version I (String-Concatenation)

team.forEach((m) => {
    text += m.anrede + '\n'
    text += m.vname + '\n'
    text += m.nname + '\n'
})

// Ausgabe Version II (Template)

team.forEach((m) => {
    text += `${
        m.anrede
    } -- ${
        m.vname
    } -- ${
        m.nname
    }   \n`
})


ausgabe.textContent = text
