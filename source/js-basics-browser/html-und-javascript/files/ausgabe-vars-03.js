const team = [
    {
        anrede: "",
        vname: "Pitti",
        nname: "Platsch"
    },
    {
        anrede: "",
        vname: "Bummi",
        nname: ""
    },
    {
        anrede: "",
        vname: "Schnatterinchen",
        nname: ""
    },
    {
        anrede: "Herr",
        vname: "",
        nname: "Fuchs"
    }, {
        anrede: "Frau",
        vname: "",
        nname: "Elster"
    }, {
        anrede: "Meister",
        vname: "",
        nname: "Nadelöhr"
    },
]

let ausgabe = document.querySelector("#debug")
let text = "" // JSON.stringify(team[5])
team.forEach((m) => {
    text += m.anrede + '\n'
    text += m.vname + '\n'
    text += m.nname + '\n'
})
// `$m.anrede   \n`
ausgabe.textContent = text
