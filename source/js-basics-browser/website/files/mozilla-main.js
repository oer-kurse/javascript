// Image switcher code

let myImage = document.querySelector('img');

myImage.onclick = function() {
  let mySrc = myImage.getAttribute('src');
  if(mySrc === 'schild-warnung-kinder-2.webp') {
    myImage.setAttribute ('src','schild-warnung-kinder-1.webp');
  } else {
    myImage.setAttribute ('src','schild-warnung-kinder-2.webp');
  }
}

// Personalized welcome message code

let myButton = document.querySelector('button');
let myHeading = document.querySelector('h1');

function setUserName() {
  let myName = prompt('Ihr Name? ');
  if(!myName) {
    setUserName();
  } else {
    localStorage.setItem('name', myName);
    myHeading.innerHTML = myName + ', JS ist mächtig!';
  }
}

if(!localStorage.getItem('name')) {
  setUserName();
} else {
  let storedName = localStorage.getItem('name');
  myHeading.innerHTML = storedName + ', JS ist mächtig!';
}

myButton.onclick = function() {
  setUserName();
}
