:PROPERTIES:
:ROAM_ALIASES: "Webserver"
:ID:           26fa53e3-f81e-4ccd-b0d8-6c9a10e14169:
:END:
#+title:       Webserver
#+date:        2024-06-01 Sa 19:08]
#+identifier:  20240601T190815
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _26fa53e3-f81e-4ccd-b0d8-6c9a10e14169:
.. INDEX:: Webserver; 
.. INDEX:: ; Webserver
#+END_export

** live-server
#+begin_src bash

  npm install live-server -g
  
#+end_src

Nach der Installation kann in der Statusleiste von VSCodium der Server
mit der aktuell bearbeiteten Seite geladen werden.

   #+begin_export rst
   
.. image:: ./images/live-server-start-stop.png
   :width: 550px
   
   #+end_export
