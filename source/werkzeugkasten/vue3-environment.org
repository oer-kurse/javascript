:PROPERTIES:
:ROAM_ALIASES: "Vitual Environment -- Python"
:ID:           7f632860-e7a8-43d0-b738-8073080aa77f:
:END:
#+title:       Vitual Environment -- Python
#+date:        2024-06-01 Sa 19:00]
#+identifier:  20240601T190029
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _7f632860-e7a8-43d0-b738-8073080aa77f:
.. INDEX:: Vitual Environment; Python & JS
.. INDEX:: Python & JS; Vitual Environment
.. index:: Vue3; Environment einrichten
#+END_export
#+BEGIN_COMMENT
https://blog.logrocket.com/new-features-in-vue-3-and-how-to-use-them/
https://medium.com/codingthesmartway-com-blog/getting-started-with-vue-cli-3-1aecf529fc71
#+END_COMMENT

** Python Environment

   Ich verwende gern Python-Environments um die Entwicklungsumgebungen vom
   System zu isolieren. Nach der einmaligen Einrichtung ist es nur ein 
   zusätzlicher Schritt: source ./env/bin/activate

   #+BEGIN_SRC bash

   mkdir vue3
   cd vue3
   python -m venv env
   source ./env/bin/activate
   #+END_SRC

** Node installieren (aus den Sourcen)

   Download Node-Source und entpacken:

   https://nodejs.org/en/download/

   #+BEGIN_SRC bash

   tar xvzf node-v12.18.2.tar.gz
   cd node-v12.18.2
   ./configure
   make -j4
   make install PREFIX=/home/javascript/vue3/env/

   node -v
   (env) /home/javascript/vue3$ 
   v12.18.2

   (env) /home/javascript/vue3$ npm -v
   6.14.5

   #+END_SRC
