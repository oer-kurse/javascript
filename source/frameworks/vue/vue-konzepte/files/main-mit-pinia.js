import { createApp } from "vue";
import { createPinia } from "pinia";

import App from "./App.vue";
import router from "./router";

const app = createApp(App)
  .use(createPinia())
  .use(router)
  .mount("#app");

// Zweite Möglichkeit der Konfiguration/Integration

// const app = createApp(App);
// app.use(createPinia());
// app.use(router);
// app.mount("#app");

