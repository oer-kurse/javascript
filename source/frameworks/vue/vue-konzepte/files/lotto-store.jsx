import { defineStore } from "pinia"

export const useLottoStore = defineStore('lottoStore', {
    state: () => ({
	lotto2021: [],
	start: 20,
	batchsize: 4
    }),

    getters: {

    },
    
    actions:{
	async fill() {
	    this.lotto2021 = (await import("@/stores/lotto2021.json")).default
	},
	jumpto(idx){
	    this.start = parseInt(idx)
	},
	prev(idx) {
	    this.start -= parseInt(idx)
	},
	next(idx){
	    this.start+= parseInt(idx)
	},
	setbatch(idx){
	    this.batchsize = parseInt(idx)
	}
    }
})
