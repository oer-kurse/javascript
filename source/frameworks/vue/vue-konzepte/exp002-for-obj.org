:PROPERTIES:
:ROAM_ALIASES: "Objekte -- als Tabelle"
:ID:           d7e46443-84cf-4e0c-b926-2368306aab39:
:END:
#+title:       Objekte -- als Tabelle
#+date:        2024-06-01 Sa 13:13]
#+identifier:  20240601T131329
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _d7e46443-84cf-4e0c-b926-2368306aab39:
.. INDEX:: Objekte; in Tabelle
.. INDEX:: in Tabelle; Objekte

.. meta::

   :description lang=de: Vue -- Konzepte: Objekte als Tabelle
   :keywords: Vue, Tabelle, Objekte, v-for

.. INDEX:: Vue; v-for
.. INDEX:: v-for; Vue

.. image:: ./images/exp-002.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../../_images/exp-002.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../../_images/exp-002.webp">
   </a>

.. sidebar:: Vue-Konzepte

   |b|

|a|

#+END_export	   

Eine Liste mit Objekten soll als Tabelle präsentiert werden.
Genutzt wird hier die v-for-Variante, die einen internen Index erzeugt.

** Aufgabe

- Erweitern Sie die tabellarische Ausgebe durch die Anzeige des Index.
- Ändern Sie Hintergrundfarbe der hervorgehobenen Zeilen auf ...


Die Syntax der v-for-Schleife:
#+begin_src javascript

  v-for="(member,idx) in team" :key="idx"
  
#+end_src


** Quellcode

Die View:  »Exp002Obj2Table.vue« 

#+begin_export rst
.. literalinclude:: files/Exp002Obj2Table.vue
   :language: html
   :linenos:
#+end_export

** Siehe auch

- https://vuejs.org/guide/essentials/list.html
