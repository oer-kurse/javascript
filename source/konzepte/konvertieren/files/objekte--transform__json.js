
let obj1 = { eins: 1, "zwei":2, "drei":3 }
let obj2 = { eins: obj1.eins, zwei: 2, drei: obj1.eins + obj1.zwei }

console.log(obj2, typeof obj2)
// { eins: 1, zwei: 2, drei: 3 }

let text = JSON.stringify(obj2)
console.log(text, typeof text)
// {"eins":1,"zwei":2,"drei":3}

let obj_from_json =  JSON.parse(text)
console.log(obj_from_json, typeof obj_from_json)
// { eins: 1, zwei: 2, drei: 3 }
