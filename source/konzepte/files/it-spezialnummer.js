function* geheimcode(code) {
  for (element of code) {
    yield element;
  }
}

let it = geheimcode("3,14159");
console.log(it.next()); // value: 1; done: false
// Warten bis zum nächsten Aufruf mit »next«
console.log(it.next());
// wieder Warten bis zum nächsten Aufruf mit »next«
console.log(it.next());
console.log(it.next());
console.log(it.next());
console.log(it.next());
console.log(it.next());

// der Iterator kann keine Werte mehr liefern...
console.log(it.next());

it = geheimcode("3,14159");
let code = "";
for (weiter of it) {
  if (weiter.done == true) break;
  code += weiter;
}
console.log(code);
