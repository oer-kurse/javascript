let ziehung = {
  [Symbol.iterator]: gen,
};

function* gen() {
  for (let i = 0; i < 6; i++) {
    yield [i + 1, Math.floor(Math.random() * 49 + 1)];
  }
}

for (let zahl of gen()) {
  console.log(zahl);
}