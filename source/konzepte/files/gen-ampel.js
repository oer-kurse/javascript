let ampel = {
  phasen: ["rot", "gelb", "grün"],
  [Symbol.iterator]: function () {
    let i = 0;
    phasen = this.phasen;
    return {
      next: function () {
        let value = phasen[i];
        i++;
        return {
          done: i > phasen.length ? true : false,
          value: value,
        };
      },
    };
  },
};

for (let phase of ampel) {
  console.log(phase);
}
