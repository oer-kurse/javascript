
function dice(min, max, anzahl) {
    let neueZahlen = []
  do {
    var x = Math.floor(Math.random() * (max - min + 1))
    if (neueZahlen.indexOf(x) < 0) {
	 neueZahlen.push(x)
    }
  } while (neueZahlen.length <= anzahl-1);
  return neueZahlen
}

module.exports = { dice }
