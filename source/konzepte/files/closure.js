// Let's imagine the next situation: a driver is sitting in a car. That
// car is inside a plane. Plane is in the airport. The ability of driver
// to access things outside his car, but inside the plane, even if that
// plane leaves an airport, is a closure.
//
// Here is how I can convert my plane story into the code.

var plane = function (defaultAirport) {

    var lastAirportLeft = defaultAirport;

    var car = {
        driver: {
            startAccessPlaneInfo: function () {
                setInterval(function () {
                    console.log("Last airport was " + lastAirportLeft);
                }, 2000);
            }
        }
    };
    car.driver.startAccessPlaneInfo();

    return {
        leaveTheAirport: function (airPortName) {
            lastAirportLeft = airPortName;
        }
    }
}("Boryspil International Airport");

plane.leaveTheAirport("John F. Kennedy");
