:PROPERTIES:
:ROAM_ALIASES: "Funktionen"
:ID:           fef55580-289e-46e3-818c-c0dec83bb0eb:
:END:
#+title:       Funktionen
#+date:        2024-06-02 So 10:44]
#+identifier:  20240602T104446
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _fef55580-289e-46e3-818c-c0dec83bb0eb:
.. INDEX:: Funktionen; 
.. INDEX:: ; Funktionen
.. index:: Funktionen
#+END_export

- Funktionen besitzen einen Datentyp, und können deshalb an Variablen
  gebunden, bzw. als Parameter weitergegeben werden.
- Parameter werden in Klammern definiert
- können einen return-Wert besitzen
  
#+begin_export rst

.. literalinclude:: files/funktionen.js
   :language: js
#+end_export

** Eine Funktionen für den Typ-Check
=================================
#+begin_export rst
.. index:: Variablen; Type-Check
.. index:: Type-Check; Variablen	      

.. literalinclude:: files/type-checks.js
   :language: js
#+end_export

** Anonyme Funktionen


Wenn eine Funktion local aufgerufen und eingebettet wird, ist keine
Name notwendig.

#+begin_src javascript

   let getData = () => {
     // Anweisungen ...
   }

   let getData = function() {
     // Anweisungen ...
   }
#+end_src

** Parameter

#+begin_src javascript

  const getData = (param1, param2) => {
    console.log(param1, param2)
  }
#+end_src

** Defaultparameter
================
::

   const getData = (farbe = 'rot',
                    baujahr = 1957) => {
     // ...
   }
   
   
Weglassen der Klammern
======================

Wenn nur eine Anweisung zum Funktionskörper gehört können die
geschweiften Klammern werggelassen werden.

::

   const getData = () => console.log('Hallo Du Pappnase!')

   
