:PROPERTIES:
:ROAM_ALIASES: "Promises"
:ID:       663358d7-2d92-4340-9b28-24513b7fbe42:
:END:
#+title:       Promises
#+date:        2024-05-31 Fr 16:36]
#+identifier:  20240531T163652
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _663358d7-2d92-4340-9b28-24513b7fbe42:
.. INDEX:: Promises; 
.. INDEX:: ; Promises
#+END_export

.. todo:: Promises

Another important aspect of E ported to JavaScript is non-blocking
promises, which E was the first language to have, Miller
said. Promises happened in ECMAScript 6 and those promises are
directly based on E promises.
  
  
*Weiterführende Links*

- https://thenewstack.io/all-about-e-the-language-that-infiltrated-javascript/
- http://www.skyhunter.com/marcs/ewalnut.html#SEC20
- https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Asynchronous/Promises
