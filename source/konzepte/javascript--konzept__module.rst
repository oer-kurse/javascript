======
Module
======


.. org::

.. _a703f48a-0010-486f-887a-598e817c51d8:
.. INDEX:: Konzept; Module
.. INDEX:: Module; Konzept

Umfangreiche Funktionalität in Funktionen und Modulen zu verteilen ist
eine gute Praxis.

Für Module stehen die Schlüsselworte export, import und required zur
Verfügung.

Zufallszahlen als Modul
-----------------------

.. literalinclude:: files/zufallszahlen.js
   :language: js

Modul nutzen
------------

.. literalinclude:: files/zufallszahlen_nutzer.js
   :language: js

Export-Varianten
----------------

Variante: jede Variable/jedes Objekt einzeln.
mit const for der Definition.

.. code:: javascript


    export const var1 = "Wert 1"
    export const var2 = "Wert 2"

Am Dateiende als Einzeiler

.. code:: javascipt


    const var1 = "Wert 1"
    const var2 = "Wert 2"

    export {var1, var2}

Ein einzelner Wert als »default«

.. code:: javascript


    const var1 = "Wert 1"
    const var2 = "Wert 2"

    export default var2

Import
------

.. code:: javascript


    import { var1, var2 } from "./konstanten.js"

wenn es einen »default«-Wert gibt dann

.. code:: javascript


    import var1 from "./konstanten.js"

Weiterführende Links:
---------------------

- `https://www.geeksforgeeks.org/import-and-export-in-node-js/ <https://www.geeksforgeeks.org/import-and-export-in-node-js/>`_

- `https://nodejs.org/api/esm.html#modules-ecmascript-modules <https://nodejs.org/api/esm.html#modules-ecmascript-modules>`_

- `https://hacks.mozilla.org/2018/03/es-modules-a-cartoon-deep-dive/ <https://hacks.mozilla.org/2018/03/es-modules-a-cartoon-deep-dive/>`_
  oder als Video: `https://www.youtube.com/watch?v=qR_b5gajwug <https://www.youtube.com/watch?v=qR_b5gajwug>`_
