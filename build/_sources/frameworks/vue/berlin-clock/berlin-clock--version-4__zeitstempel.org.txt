:PROPERTIES:
:ROAM_ALIASES: "IV. Zeitstempel"
:ID:           3b05a317-d9b6-4985-bca0-1904113479d7:
:END:
#+title:       IV. Zeitstempel
#+date:        2024-06-01 Sa 14:58]
#+identifier:  20240601T145842
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _3b05a317-d9b6-4985-bca0-1904113479d7:
.. INDEX:: IV. Zeitstempel; 
.. INDEX:: ; IV. Zeitstempel
.. index:: Berlin-Clock; Schritt IV
#+END_export

Wenn die Testausgabe funktioniert, müssen die einzelnen Werte
ausgewertet werden. Zu diesem Zweck speichern wir diese ab, um danach
Vergleiche anstellen zu können.

Die Abarbeitung muss dann ein wenig umgestellt werden:

- Daten ablegen in **zeitstempel** (Zeilen 44-48)
- return durch Zuweisung ersetzten (Zeile 85)
- Testausgabe nicht mehr durch Funktionsaufruf, sondern die Ausgabe
  des Variableninhaltes aus dem Data-Block (Zeile 37).

[[./images/berlin-clock-schritt-04.png]]

#+begin_export rst

.. literalinclude:: ./files/berlinclock4.vue
   :language: html
   :linenos:
   :emphasize-lines: 84-87, 93-96, 134
   
#+end_export
