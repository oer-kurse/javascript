:PROPERTIES:
:ROAM_ALIASES: "II. Bilder positionieren"
:ID:       ebc46844-ed3e-4c44-8ac5-781746bfebbe:
:END:
#+title:       II. Bilder positionieren
#+date:        2024-06-01 Sa 14:48]
#+identifier:  20240601T144813
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _ebc46844-ed3e-4c44-8ac5-781746bfebbe:
.. INDEX:: II.; Bilder positionieren
.. INDEX:: Bilder positionieren; II.
.. index:: Berlin-Clock; CSS

#+END_export

Die korrekte Positionierung der Bilder erfolgt über CSS-Anweisungen.
Sind alle Bilder am richtigen Platz werden Sie gleich noch versteckt
und nur das Hintergrundbild ist zu sehen. Ein Test der korrekten Positionierung
ist über das CSS-Attribut: *visibility* möglich. Der Austausch der 
Anweisung *visibility:hidden* durch *visibility:visible*, sollte das
betreffende Elemente am richtigen Platz sichtbar werden. 

[[./images/berlin-clock-schritt-02.png]]

#+begin_export rst

.. literalinclude:: ./files/berlinclock2.vue
   :language: html
   
#+end_export
