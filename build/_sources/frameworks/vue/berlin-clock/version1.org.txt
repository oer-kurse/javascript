:PROPERTIES:
:ROAM_ALIASES: "Bilder für die Uhr"
:ID:       e3807225-33fd-44ce-9ccf-dc1d1665828b:
:END:
#+title:       I.  Bilder für die Uhr
#+date:        2024-05-31 Fr 18:36]
#+identifier:  20240531T183611
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _e3807225-33fd-44ce-9ccf-dc1d1665828b:
.. INDEX:: Berlin-Clock I: Bilder; für die Uhr
.. INDEX:: für die Uhr; Berlin-Clock I
#+END_export

Die Uhr wird aus einzelnen Grafiken zusammengesetzt. Die Bilder für die Uhr als
Download:
#+begin_export rst
`./files/clockimages.zip <./files/clockimages.zip>`_
#+end_export

- Diese speichern Sie im Ordner *public* des Projektes.
- Vergessen Sie nicht, die Namen *berlinclock1.vue* in **App.vue** zu importieren.

Wie am Bildschirmfoto zu erkennen ist, sind noch nicht alle Bilder am
richtigen Platz. Die Lösung wird auf der nächsten Seite erklärt.

[[./images/berlin-clock-schritt-01.png]]

#+begin_export rst
.. literalinclude:: ./files/berlinclock1.vue
   :language: html
#+end_export
