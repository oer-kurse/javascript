:PROPERTIES:
:ROAM_ALIASES: "V. Anzeige aktivieren"
:ID:       10146a9d-b323-400b-9ea9-c52a5ad05154:
:END:
#+title:       V. Anzeige aktivieren
#+date:        2024-06-01 Sa 15:09]
#+identifier:  20240601T150953
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _10146a9d-b323-400b-9ea9-c52a5ad05154:
.. INDEX:: V. Anzeige aktivieren; 
.. INDEX:: ; V. Anzeige aktivieren
.. index:: Berlin-Clock; Version V
#+END_export

Nun muss für jedes Bild entschieden werden, ob es sichtbar sein soll
oder eben nicht. Gesteuert wird es über die Wahrheitswerte in der
Struktur *timestamp*. Eine neue Funktion führt nun den Vergleich durch
und gibt als Ergebnis die passende CSS-Anweisung zurück. Für jedes
Bild wird dann die Funktion aufgerufen. Hier ein Beispiel aus dem
Skript:


   #+begin_src html
   <img id="minute04" 
        src="/static/clockimages/minute-right.png"
        v-bind:style="isActive('minute04')"/>
   #+end_src

Die Funktion *isActive* befindet sich im Methoden-Block (Zeilen
74-76). Dort verwenden wir den bedingten Operatator (?:) um eine
wenn-dann-Entscheidung treffen zu können.
Damit die aktuelle Zeit auch wirklich angezeigt wird, brauchen wir
noch eine Initialzündung. Das machen wir in der Zeile 61, indem wir die
Zeitermittlung einmalig aufrufen.

[[./images/berlin-clock-schritt-05.png]]

#+begin_export rst
	   
.. literalinclude:: ./files/berlinclock5.vue 
   :language: html
   :linenos:
   :emphasize-lines: 7,9,11, 77-79
#+end_export
