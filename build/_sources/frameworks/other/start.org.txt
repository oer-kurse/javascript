:PROPERTIES:
:ROAM_ALIASES: "React -- Vite"
:ID:           0376f53c-c40a-4781-9e2a-e39744e9f99c:
:END:
#+title:       React -- Vite
#+date:        2024-05-31 Fr 17:15]
#+identifier:  20240531T171529
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _0376f53c-c40a-4781-9e2a-e39744e9f99c:
.. INDEX:: React; 
.. INDEX:: ; React
#+END_export

Vite unterstützt nicht nur Vue sondern auch andere Frameworks, so auch »React«.

#+begin_src bash
  yarn create vite
#+end_src

   
