:PROPERTIES:
:ROAM_ALIASES: "React"
:ID:           b8df7bb8-d51b-402b-88f4-9607a9b016a5:
:END:
#+title:       React --  -- 
#+date:        2024-05-31 Fr 17:11]
#+identifier:  20240531T171129
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _b8df7bb8-d51b-402b-88f4-9607a9b016a5:
.. _js-react-start:
.. INDEX:: React; 
.. INDEX:: ; React
#+END_export

Meine ersten Erfahrungen mit React, führten eher zu einer Ablehnung.
Deshalb verfolge ich die Entwicklungen nicht weiter und kann React
auch nicht empfehlen (sicher eine ganz persönliche Empfindung/Erfahrung).

Der folgende Artikel bestätigt meine Erfahrungen mit Gatsby. Die
Produtivität ist im Eimer..., Fehler finden, ist ein Horror...,
Übersetzungsläufe extrem langsam ...
   
- [[https://www.twopicode.com/articles/this-is-why-we-stopped-using-react-native/][Stopp using React native]]
- [[<https://joshcollinsworth.com/blog/antiquated-react>][Ein Vergleich: React und andere Frameworks]]


.. toctree::
   :maxdepth: 1
   :caption: 📘
   :glob:

   *

.. toctree::
   :maxdepth: 1
   :caption: 📁
   :glob:
   
