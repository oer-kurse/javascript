:PROPERTIES:
:ROAM_ALIASES: "Konvertieren -- toInteger"
:ID:       80732fa7-4d40-4e78-ac2d-0fa37a0adad0:
:END:
#+title:       Konvertieren -- toInteger
#+date:        2024-05-25 Sa 20:04]
#+identifier:  20240525T200401
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _80732fa7-4d40-4e78-ac2d-0fa37a0adad0:
.. INDEX:: Konvertieren; toInteger
.. INDEX:: toInteger; Konvertieren
.. INDEX:: Konvertieren; parseInt
.. INDEX:: parseInt; Konvertieren

#+END_export

Die Funktion heißt nicht toInteger, sondern /ParseInt/ !

*Aufgabe:* Wandeln Sie den Zweizeiler ab.

*Frage:* Was passiert bei der Umwandlung, wenn zwei Zahlen im Eingabestring enthalten sind? 

** String in Zahl wandeln:

#+begin_src javascript

  let input = parseInt("111 ist die Eingabe aus einem Formular")
  console.log(input, typeof input)
  
#+end_src

