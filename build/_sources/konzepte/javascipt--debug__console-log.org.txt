:PROPERTIES:
:ROAM_ALIASES: "Debug"
:ID:       286c8d7a-d064-48f2-8f36-51c353c7c2aa:
:END:
#+title:       Debug
#+date:        2024-05-26 So 10:22]
#+identifier:  20240526T102234
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _286c8d7a-d064-48f2-8f36-51c353c7c2aa:
.. INDEX:: Debug
.. INDEX:: debug; Fehlersuche
.. INDEX:: Fehlersuche; debug
#+END_export

*console.log*
#+begin_src javascript

  console.log("test-Ausgabe als Text bzw. Ausgabe von Variablen")
  
#+end_src

In den Developertools dann den Debugbereich nutzen...
  
*Use strict*
	   
Als erste Zeile einer Javascript-Datei:

   #+begin_src javascript
   'use strict'
   #+end_src

Es wird gewarnt vor:

- nicht deklarierte Variablen
- die Nutzung von reservierten Worten
- löschen von Variablen, wenn es nicht erlaubt ist
- löschen einer Funktion, wenn es nicht erlaubt ist
- Doppelt deklarierte Parameter
- Einschränkungen bei der Verwendung der Funktion eval()

*Weiterführende Links*

- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode
