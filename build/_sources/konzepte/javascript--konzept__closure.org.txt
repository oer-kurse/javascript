:PROPERTIES:
:ROAM_ALIASES: "Closures"
:ID:           df8ecd4c-5dea-4a9a-8dd7-d2c5406960c9:
:END:
#+title:       Closures
#+date:        2024-05-26 So 10:01]
#+identifier:  20240526T100126
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _df8ecd4c-5dea-4a9a-8dd7-d2c5406960c9:
.. INDEX:: Closures
#+END_export

*Python*

- http://ballingt.com/python-closures
  
*JavaScript*

:viele Antworten auf:

- http://stackoverflow.com/questions/111102/how-do-javascript-closures-work
- http://sleeplessgeek.blogspot.com/2009/12/so-what-are-these-closure-thingys.html (beste Antwort)

*Beste Antwort*[fn:1]


Closures sind eine Möglichkeit, eine Funktion persistent zu machen,
private Variablen - d.h. Variablen, die nur eine Funktion kennt und
in denen sie Informationen aus früheren Ausführungszeiten speichern kann.


Eine Rechenfunktion die addieren und multiplizieren kann:

#+begin_src javascript

 
   function make_calculator() {
     var n = 0;  // this calculator stores a single number n
     return {
       add : function (a) { n += a; return n; },
       multiply : function (a) { n *= a; return n; }
     };
   }

   first_calculator = make_calculator();
   second_calculator = make_calculator();

   first_calculator.add(3);                   // returns 3
   second_calculator.add(400);                // returns 400

   first_calculator.multiply(11);             // returns 33
   second_calculator.multiply(10);            // returns 4000
#+end_src


Der springende Punkt: Jeder Aufruf von make\under{}calculator erzeugt eine
neue lokale Variable n, die von den Additions- und
Multiplikationsfunktionen dieses Rechners noch lange nach der Rückkehr
von make\under{}calculator verwendet werden kann.


Wenn Sie mit Stackframes vertraut sind, erscheinen Ihnen diese Rechner
seltsam:

Wie können sie nach der Rückkehr von make\under{}calculator weiterhin auf n
zugreifen? Stellen Sie sich vor, dass JavaScript keine »Stack-Frames« 
verwendet, sondern »Heap-Frames«, die auch nach der Rückkehr des
Funktionsaufrufs, der sie erzeugt hat, weiter bestehen können.

Innere Funktionen wie Addieren und Multiplizieren, die auf Variablen
zugreifen, die in einer äußeren Funktion deklariert wurden, werden
»Closures« genannt.

*Wikipedia*

In computer science, a closure is a function together with a
referencing environment for the nonlocal names (free variables) of
that function.

* Footnotes

[fn:1] Übersetzt mit DeepL.com
