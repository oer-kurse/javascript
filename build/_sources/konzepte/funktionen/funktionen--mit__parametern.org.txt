:PROPERTIES:
:ROAM_ALIASES: "Funktionen -- mit -- Parametern"
:ID:       c7c24668-8941-46d8-9a91-9c3fae001244:
:END:
#+title:       Funktionen -- mit -- Parametern
#+date:        2024-05-26 So 12:36]
#+identifier:  20240526T123600
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _c7c24668-8941-46d8-9a91-9c3fae001244:
.. INDEX:: Funktionen; Parameter
.. INDEX:: Parameter; Funktionen
#+END_export
   #+begin_src javascript
   
   const getData = (param1, param2) => {
     console.log(param1, param2)
   }
   #+end_src

*Defaultparameter*

#+begin_src javascript

   const getData = (farbe = 'rot',
                    baujahr = 1957) => {
     // ...
   }
#+end_src

*Beliebig viele Werte als Parameter*

#+begin_export rst

- :ref:`Siehe Beispiele zum Spread-Operator <30a24a36-caaa-4153-9483-9eecd5846282>`
#+end_export

   
**Weglassen der Parameter*

Wenn nur ein Parameter übergeben wird, können die runden Klammern weggelassen
werden:

#+begin_src javsscript

   const getData = param => console.log(param)
  
#+end_src
