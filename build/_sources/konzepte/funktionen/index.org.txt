:PROPERTIES:
:ROAM_ALIASES: "Funktionen"
:ID:           29b19316-2b47-49c2-a426-78c8c2b37132:
:END:
#+title:       Funktionen
#+date:        2024-05-26 So 12:18]
#+identifier:  20240526T121835
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _29b19316-2b47-49c2-a426-78c8c2b37132:
.. INDEX:: Funktionen; 
.. INDEX:: ; Funktionen

.. toctree::
   :maxdepth: 1
   :caption: 📁
   :glob:
   
   */index

.. toctree::
   :maxdepth: 1
   :caption: 📘
   :glob:
   
   *
#+END_export

