#+TITLE: Virtual Environment
#+CATEGORY: Vue3/Python
#+STARTUP: overview

#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil

#+index: Python; Environment einrichten (venv)

* Python (venv), warum?

  Ich verwende gern Python-Environments um die Entwicklungsumgebungen vom
  System zu isolieren. Nach der einmaligen Einrichtung ist es nur ein 
  zusätzlicher Schritt: 
  #+BEGIN_SRC bash

    source ./env/bin/activate
  
  #+END_SRC

* Einrichten (einmalig)  

  #+BEGIN_SRC bash

    mkdir vue3
    cd vue3
    python -m venv env
    source ./env/bin/activate

  #+END_SRC


* Aktivieren (immer wieder)  

  Vor der Arbeit im virtuellen Environment...

  #+BEGIN_SRC bash

    cd vue3
    source ./env/bin/activate

  #+END_SRC
