=====
 SWC
=====

SWC ist eine erweiterbare, auf Rust basierende Plattform für die
nächste Generation von schnellen Entwickler-Tools. Sie wird von Tools
wie Next.js, Parcel und Deno sowie von Unternehmen wie Vercel,
ByteDance, Tencent, Shopify und anderen verwendet.

SWC kann sowohl zum Kompilieren als auch zum Bündeln verwendet
werden. Bei der Kompilierung werden JavaScript-/TypeScript-Dateien mit
modernen JavaScript-Funktionen verwendet und gültiger Code ausgegeben,
der von allen wichtigen Browsern unterstützt wird.

Übersetzt mit www.DeepL.com/Translator (kostenlose Version)

- Website: https://swc.rs/
