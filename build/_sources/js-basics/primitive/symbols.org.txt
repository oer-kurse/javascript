:PROPERTIES:
:ROAM_ALIASES: "Symbole"
:ID:           89901dbf-895a-4916-8a85-b8d7546cd1e1:
:END:
#+title:       Symbole
#+date:        2024-05-24 Fr 17:39]
#+identifier:  20240524T173920
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _89901dbf-895a-4916-8a85-b8d7546cd1e1:
.. INDEX:: Primitve; Symbole
.. INDEX:: Symbole; Primitve

.. meta::

   :description lang=de: JavaScript -- Symbole
   :keywords: JavaScript, Symbole 

.. image:: ./images/letzter-junker-in-dahn.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/letzter-junker-in-dahn.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/letzter-junker-in-dahn.webp">
   </a>

.. sidebar:: JavaScript-Kurs

   |b|

   Letzter Junker in Dahn

|a|

#+END_export

- Identifier
- erlaubt Objekte mit Properties zu ergänzen
- besitzen nicht die Gefahr des Überschreibens

#+begin_src javascript


   let symbol = Symbol('debug');

   console.log(symbol);
   console.log(typeof symbol);

   let obj = {

     naem: 'Heinz',
     [symbol]: 555
   }

   console.log(obj[symbol]);
#+end_src

   
*Zwei Symbole für eine ID*

#+begin_src javascript

   let symbol01 = Symbol.for('alter');
   let symbol02 = Symbol.for('alter');

   console.log(symbol01 === symbol02); // true
#+end_src

 aber

 
#+begin_src javascript

    let Sym1 = Symbol("Sym")
    let Sym2 = Symbol("Sym")

    console.log(Sym1 === Sym2) // false
#+end_src

*Weiterführende Links*

- https://www.programiz.com/javascript/symbol
  
 
 
