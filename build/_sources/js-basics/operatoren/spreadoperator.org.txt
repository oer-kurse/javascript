:PROPERTIES:
:ROAM_ALIASES: "Spread-Operator -- (...)"
:ID:           30a24a36-caaa-4153-9483-9eecd5846282:
:END:
#+title:       Spread-Operator -- (...) 
#+date:        2024-05-22 Mi 20:12]
#+identifier:  20240522T201251
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org
[[id:27470f9f-90e1-4dab-a792-9c70c2dac1c4][Operatoren]]
#+end_org

#+BEGIN_export rst
.. _30a24a36-caaa-4153-9483-9eecd5846282:
.. INDEX:: Spread-Operator; (...)
.. INDEX:: (...); Spread-Operator

.. image:: ./images/groester-schuh.webp
   :width: 0px


.. |a| raw:: html

   <!-- lightbox container hidden with CSS -->
   <a href="#" class="lightbox" id="img1">
   <span style="background-image: url('../../_images/groester-schuh.webp')"></span>
   </a>

.. |b| raw:: html

   <a href="#img1">
   <img width="250px" src="../../_images/groester-schuh.webp">
   </a>

.. sidebar:: JavaScript-Kurs

   |b|

   Wer hat den Größten?

|a|

#+END_export

Die Spread-Syntax ermöglicht:

- Ein Iterable (z.B. Array oder eine Zeichenkette) zu expandieren:
  - Bei Funktionsaufrufen (null oder mehr Argumente)
  - Bei Array-Literalen (null oder mehr Elemente)
- Ein Objekt zu expandieren:    
  - Bei Objekt-Literalen (null oder mehr Schlüssel-Wert-Paare)

#+begin_src javascript

    function summe(...zahlen) {
      console.log(zahlen);
      let sum = 0;
      for (let i = 0; i < zahlen.length; i++) {
        sum += zahlen[i];
      }
      return `Die Summe ist: ${sum}`;
    }

    // Liste wird in Array gewandelt
    console.log(summe(9, 8, 7, 6, 5, 4, 3, 2, 1)); 
  
#+end_src

Zweites Beispiel, die Anzahl der Teammitglieder kann variieren.


   #+begin_src javascript
   const printProjekt = (projName, projLeiter, ...projTeam) => {
     console.log(`Projekt-Bezeichnung: ${projName}`);
     console.log(`Projekt-Leiter: ${projLeiter}`);
     console.log(`Projekt-Team: ${projTeam.join(",")}`);
   };

   const projekt = {
     name: "Diggedags",
     leiter: "-",
     team: ["Dig", "Dag", "Digedag"],
   };

   printProjekt(projekt.name, projekt.leiter, projekt.team);
   #+end_src



*Weiterführende Links*

- https://learnersbucket.com/tutorials/es6/spread-and-rest-operators-in-javascript/
 
 
