:PROPERTIES:
:ROAM_ALIASES: "Logische"
:ID:           6d42cab7-f314-41cb-8bc8-38491acc7e9f
:END:
#+title:       Logische
#+date:        [2024-05-09 Do 10:04]
#+identifier: 20240509T100446
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org
[[id:27470f9f-90e1-4dab-a792-9c70c2dac1c4][Operatoren]]
#+end_org
#+BEGIN_export rst
.. _6d42cab7-f314-41cb-8bc8-38491acc7e9f:
.. INDEX:: Operatoren; logische 
.. INDEX:: logische; Operatoren
.. INDEX:: Operatoren; ternary
.. INDEX:: ternary; Operatoren
#+END_export

Die Kombination logischer Verknüpfungen ergibt eine Aussage, die für
den Programmfluss genutzt werden kann.

#+begin_src javascript
   a && b   // und
   a || b   // oder
   !a       // Verneinung/Negation
   ?:       // Kondition (conditional/ternary operator)
   x > 0 ? x*y : -x*y
#+end_src
   
   
 
 
