:PROPERTIES:
:ROAM_ALIASES: "JavaScript -- einbinden"
:ID:       cbb636fc-df28-45e2-82f9-a353bd65985a:
:END:
#+title:       JavaScript -- einbinden
#+date:        2024-06-01 Sa 21:06]
#+identifier:  20240601T210606
#+OPTIONS: num:nil
#+OPTIONS: author:nil
#+OPTIONS: toc:nil
#+OPTIONS: date:nil
#+begin_org

#+end_org
#+BEGIN_export rst
.. _cbb636fc-df28-45e2-82f9-a353bd65985a:
.. INDEX:: JavaScript; einbinden
.. INDEX:: einbinden; JavaScript
#+END_export

JavaScirpt wird im HEAD oder alternativ am Seitenende eingebunden.
Im Beispiel ist auch schon ein »pre«-Element eingefügt, um Testausgaben
platzierenn und in der Website sichtbar zu machen.

#+begin_export rst

.. literalinclude:: ./files/ausgabe-vars-02.html	     
   :language: html
   :linenos:

#+end_export

** Testausgaben

Die in den Grundlagen oft genutzte Anweisung »console.log« kann nicht
in den Browser umgelenkt werden.  Es muss das Element ausgewählt
werden, welches manipuliert werden soll. Hat man eine Variable und
gibt hinter dem Namen einen Punkt ein, sollte ein guter Editor
Vorschläge machen, was alles möglcih ist...

Im Beispiel setzen wir diverse Variablen zu einem Text zusammen und
ordnen den Text dann mit der Methode »textContent« dem »pre«-Element
zu.

Die Zeichenfolge »\\n« erzeugt einen Zeilenumbruch, was auch nur
»pre«-Element beachtet wird.  
#+begin_export rst

.. literalinclude:: ./files/ausgabe-vars-02.js	     
   :language: javascript
   :linenos:
   :emphasize-lines: 20- 25
#+end_export
		     
** Console.log

Kann weiter genutzt werden. Über die Menüstruktur des Browsers
bzw. das Kontextmenü (rechter Mausklick) können die »Developer«-Tools
geöffent werden. Dort sind unter dem Karteireiter »Console« auch die
console.log-Ausgaben einsehbar.
		     
